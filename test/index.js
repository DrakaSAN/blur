'use strict';

const { parseArgs, blur, checkArgs } = require('../index');
const jimp = require('jimp');
const fs = require('fs/promises');
const sinon = require('sinon');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinonChai = require('sinon-chai');
const path = require('path');

chai.use(chaiAsPromised);
chai.use(sinonChai);

const { expect } = chai;

describe('index', () => {
	describe('parseArgs', () => {
		afterEach(() => {
			sinon.restore();
		});

		it('apply defaults arguments', () => {
			sinon.stub(path, 'resolve').returnsArg(0);
			sinon.stub(process, 'cwd').returns('cwd');

			const args = parseArgs();

			expect(args).to.include({
				input: 'cwd',
				output: 'cwd' + path.sep + 'out',
				blur: 7
			});
		});

		it('understands all flags', () => {
			sinon.stub(process, 'cwd').returns('cwd');
			const args = parseArgs(['-i', '/input', '-o', '/output', '-b', 219]);

			expect(args).to.include({
				input: '/input',
				output: '/output',
				blur: 219
			});
		});

		it('defaults to main arg if input flag is not used', () => {
			sinon.stub(process, 'cwd').returns('cwd');
			const args = parseArgs(['/input']);

			expect(args).to.include({
				input: '/input',
				output: '/input/out',
				blur: 7
			});
		});

		it('refuses identical input/output paths', () => {
			expect(() => parseArgs(['-i', 'test', '-o', 'test'])).to.throw('Invalid output folder');
		});

		it('allows overwriting if --force flag is set', () => {
			sinon.stub(process, 'cwd').returns('cwd');
			const args = parseArgs(['-i', '/test', '-o', '/test', '--force']);

			expect(args).to.include({
				input: '/test',
				output: '/test',
				blur: 7
			});
		});
	});

	describe('checkArgs', () => {
		afterEach(() => {
			sinon.restore();
		});

		it('exits with a warning if the input folder is not detected', async () => {
			const error = new Error('Oops');
			error.code = 'ENOENT';
			sinon.stub(fs, 'access').withArgs('input').rejects(error);

			await expect(checkArgs({ input: 'input' })).to.eventually.be.rejectedWith(error);
		});

		it('creates the output folder if needed', async () => {
			const error = new Error('Oops');
			error.code = 'ENOENT';
			sinon.stub(fs, 'access').withArgs('input').resolves().withArgs('output').rejects(error);
			sinon.stub(fs, 'mkdir');

			await checkArgs({ input: 'input', output: 'output' });
			expect(fs.mkdir).to.have.been.calledWith('output');
		});
		
		it('do nothing if the output folder exists', async () => {
			const error = new Error('Oops');
			error.code = 'ENOENT';
			sinon.stub(fs, 'access').resolves();
			sinon.stub(fs, 'mkdir').resolves();
			
			await checkArgs({ input: 'input', output: 'output' });
			expect(fs.mkdir).to.not.have.been.called;
		});
	});

	describe('blurFolder', () => {
		it('fetch all images files from the input folder', () => {
			
		});

		it('blur all images', () => {

		});
	});

	describe('blur', () => {
		it('fetch all images files from the input folder', () => {

		});

		it('blur all images', () => {

		});

		it('saves the images in the output folder', () => {

		});
	});
});