#!/usr/bin/env node

'use strict';

const fs = require('fs/promises');
const jimp = require('jimp');
const yargs = require('yargs');
const { hideBin } = require('yargs/helpers');
const path = require('path');

/**
 * @typedef Args
 * @property {string} input
 * @property {string} output
 * @property {number} blur 
 */

/**
 * @param {string[]} argv - process.argv
 * @returns {Args}
 */
function parseArgs(argv) {
	const args = yargs
		.option('input', {
			alias: 'i', type: 'string', description: 'input folder, all images detected in it will be copied and blurred'
		})
		.option('output', {
			alias: 'o', type: 'string', description: 'output folder, defaults to a folder named "out" in the input folder'
		})
		.option('blur', {
			alias: 'b', type: 'number', description: 'By how many pixels to blur (defaults to 7)', default: 7
		})
		.option('force', {
			alias: 'f', type: 'boolean', description: 'Allow to overwrite the original images, DO NOT USE unless you know what you are doing.', default: false
		})
		.parse(argv);

	if (!args.input) {
		if (args._.length > 0) {
			args.input = args._[0];
		} else {
			args.input = process.cwd();
		}

	}
	args.input = path.resolve(args.input);
	args.output = (args.output) ? path.resolve(args.output) : path.join(args.input, 'out');

	if (args.input === args.output) {
		console.log('Input and output folder are the same, the original images will be overwritten');
		if (!args.force) {
			console.log('If you truly want that, use the --force flag');
			throw new Error('Invalid output folder');
		}
	}
	return args;
}

/**
 * @param {Args} args
 * @returns {Promise<Args>}
 */
async function checkArgs(args) {
	await fs.access(args.input);
	try {
		await fs.access(args.output);
	} catch(error) {
		if (error.code !== 'ENOENT') {
			throw error;
		}
		await fs.mkdir(args.output);
	}
	return args;
}

/**
 * @param {Args} args
 * @returns {Promise<string[]>}
 */
async function blurFolder(args) {
	const imageExtensions = ['.png', '.jpg', '.jpeg', '.bmp', '.tiff', '.gif'];

	const files = await fs.readdir(args.input);
	const imageFiles = files
		.filter((file) => imageExtensions.some((ext) => file.endsWith(ext)));
	console.log(`Files detected:\n- ${imageFiles.join('\n- ')}`);
	return Promise.all(imageFiles.map((file) => blur(file, args)));
}

/**
 * @param {string} file
 * @param {Args} args
 * @returns {Promise<string>}
 */
async function blur(file, args) {
	const input = path.join(args.input, file);
	const output = path.join(args.output, file);

	try {
		const image = await jimp.read(input);
		await image
			.clone()
			.blur(args.blur)
			.write(output);
	} catch (error) {
		console.error(`Error when blurring ${input}`);
	}

	return output;
}

if (require.main === module) {
	const args = parseArgs(hideBin(process.argv));
	console.log(`Converting files from ${args.input} to ${args.output}`);
	Promise.resolve(args)
		.then(checkArgs)
		.then(blurFolder)
		.then((output) => {
			console.log('Done');
			console.log(output.join('\n'));
		})
		.catch((error) => {
			console.error(error);
		});
}

module.exports = {
	parseArgs,
	checkArgs,
	blurFolder,
	blur
};