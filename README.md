# Blur

[![pipeline status](https://gitlab.com/DrakaSAN/blad/badges/dev/pipeline.svg)](https://gitlab.com/DrakaSAN/blad/commits/dev) [![coverage report](https://gitlab.com/DrakaSAN/blad/badges/dev/coverage.svg)](https://gitlab.com/DrakaSAN/blad/commits/dev)

Tool to quickly blur all image files in a folder.

[Notice en francais](./README-fr.md)

[![pipeline status](https://gitlab.com/DrakaSAN/blur/badges/master/pipeline.svg)](https://gitlab.com/DrakaSAN/blur/-/commits/master) [![coverage report](https://gitlab.com/DrakaSAN/blur/badges/master/coverage.svg)](https://gitlab.com/DrakaSAN/blur/-/commits/master) 

## Installation

### Prerequisite

Ensure you have [node.js](https://nodejs.org/en/) installed. You can check that by opening a terminal and typing `node -v`, if node is installed correctly, you'll have a message like `v14.15.0`.

### The script itself (optionnal)

In a terminal, run:

```sh
npm install -g @d219/blur
```

Note that it isn't neccessary to install before use, as the `run` step will make a temporary install if needed, but it will need to connect to internet to do so.

## Run

```sh
npx @d219/blur --input path/to/input/folder --output path/to/output/folder
```

This will make a temporary install if needed, then convert all image files detected in `path/to/input/folder` and put the blurred images in `path/to/output/folder` with the same names.

If you installed the script, the command can be simplified:

```sh
blur --input path/to/input/folder --output path/to/output/folder
```
