# Blur

Outils pour rapidement flouter toutes les images d'un dossier.

[Notice in english](./README.md)

[![pipeline status](https://gitlab.com/DrakaSAN/blur/badges/master/pipeline.svg)](https://gitlab.com/DrakaSAN/blur/-/commits/master) [![coverage report](https://gitlab.com/DrakaSAN/blur/badges/master/coverage.svg)](https://gitlab.com/DrakaSAN/blur/-/commits/master) 

## Installation

### Prérequis

Assurez vous d'avoir [node.js](https://nodejs.org/fr/) d'installé. Pour vous en assurer, vous pouvez ouvrir un terminal et taper `node -v`, si l'installation est complète, vous verrez un message comme `v14.15.0`.

### Le script lui même (optionnel)

Dans un terminal, lancez:

```sh
npm install -g @d219/blur
```

À noter que cette installation est optionnelle, puisque l'execution fera une installation temporaire si besoin, en revanche, il vous faudra une connexion internet pour cela.

## Utilisation

```sh
npx @d219/blur --input dossier/a/flouter --output dossier/flouté
```

Si besoin, cela fera une installation temporaire, puis cela foutera toutes les images détectées dans `dossier/a/flouter` et mettra les images floutées dans `dossier/flouté` avec les mêmes noms.

Si le script est installé, la commande peux être simplifiée:

```sh
blur --input dossier/a/flouter --output dossier/flouté
```
